﻿using System.IO;
using System;


namespace TextEditor{
    class Program{
        static void Main(){
            Menu();
        }

        static void Menu(){
            Console.Clear();
            Console.WriteLine("Select an option");
            Console.WriteLine("1 - Open file");
            Console.WriteLine("2 - Create file");
            Console.WriteLine("0 - Exit");
            short option = short.Parse(Console.ReadLine());

            switch(option){
                case 0: 
                    System.Environment.Exit(0);
                    break;
                case 1: 
                    Open(); 
                    break;
                case 2: 
                    Edit();
                    break;
                default: 
                    Menu();
                    break;
            }
        }

        static void Open(){
            Console.Clear();
            System.Console.WriteLine("Type the path of the file:");
            string path = Console.ReadLine();
            
            using(var file = new StreamReader(path)){
                string text = file.ReadToEnd();
                System.Console.WriteLine(text);
            }
            System.Console.WriteLine("");
            Console.ReadLine();
            Menu();
        }

        static void Edit(){

            Console.Clear();
            System.Console.WriteLine("Type your text: (ESC to exit)");
            System.Console.WriteLine("------------------------");
            string text = "";

            do{
                text += Console.ReadLine();
                text += Environment.NewLine;

            }while(Console.ReadKey().Key != ConsoleKey.Escape);
            

            Save(text);
        }

        static void Save(string text){
            Console.Clear();
            System.Console.WriteLine("Directory to save the file:");
            var path = Console.ReadLine();
            
            using(var file = new StreamWriter(path)){
                file.Write(text);
            }

            System.Console.WriteLine($"File {path} saved");
            Console.ReadLine();
            
            Menu();

        }
        
    }
        
}
