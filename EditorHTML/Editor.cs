using System.Text;
using System;

namespace  EditorHTML{
    public static class Editor{
        public static void Show(){
            Console.Clear();
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Clear();
            Console.WriteLine("Editor Mode");
            System.Console.WriteLine("------------------------------");
            Start();
        }
        public static void Start(){
            string file = "";
            

            do{
                file += Console.ReadLine();
                file += Environment.NewLine;
            }while(Console.ReadKey().Key != ConsoleKey.Escape);

            System.Console.WriteLine("------------------------------");
            Save(file);

        }

        static void Save(string text){
            Console.Clear();
            System.Console.WriteLine(" Saving file");
            System.Console.WriteLine("Directory to save the file:");
            var path = Console.ReadLine();
            
            using(var file = new StreamWriter(path)){
                file.Write(text);
            }

            System.Console.WriteLine($"File {path} saved");
            Console.ReadLine();
            
            Menu.Show();
        }
    }
        
}