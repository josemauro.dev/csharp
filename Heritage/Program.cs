﻿using System;

namespace Vehicle{
    class program{
        static public void Main(){
            string vModel, vColor;
            int vWheels;
            var car = new Car();
            car.Characteristics(out vModel, out vColor, out vWheels);
            car.Show(vModel, vColor, vWheels);
        }
    }
}