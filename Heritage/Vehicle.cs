using System.Dynamic;
using System.Globalization;
using System;

namespace Vehicle{
     public class VehicleType{
      public static int MaxSpeed;
      public static bool OnOff;
        
      public static bool setOnOFF(){
         var option = "0";
         bool key=false;
         
           
            System.Console.WriteLine("The car is off, turn it on?");
            System.Console.WriteLine("0 - No | 1 - Yes");
            option = Console.ReadLine();
            if (option == "0" ){
               key = false;
               System.Console.WriteLine("The engine is now on {0} state(OFF)", key);
            }
            else if(option == "1"){
               key = true;
               System.Console.WriteLine("The engine is now on {0} state(ON)", key);
               
            }
            else{
            System.Console.WriteLine("Invalid option");
            setOnOFF();
            }
            return key;
         }
      public static string setMaxSpeed(){
         string speed="";
         System.Console.WriteLine("Type the max speed of the car: ");
         speed = Console.ReadLine();
         
         return speed;
      }

      
      public static void TurnON(){
         VehicleType.setOnOFF();
         VehicleType.setMaxSpeed();
        }
     }
}
