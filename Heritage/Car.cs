using System;

namespace Vehicle{
    public class Car:VehicleType{
        public string model="";
        public string color="";
        public int wheels=0;
        
        public void Show(string sModel, string sColor, int sWheels){
            Console.Clear();
            string sSpeed;
            bool sOnOFF = false;
            System.Console.WriteLine("The model of the Vehicle is: {0}", sModel);
            System.Console.WriteLine("The color of the Vehicle is: {0}", sColor);
            System.Console.WriteLine("The number of wheels on the Vehicle is: {0}", sWheels);
            sSpeed = VehicleType.setMaxSpeed();
            sOnOFF = VehicleType.setOnOFF();
            if(sOnOFF = false){
                System.Console.WriteLine("The {0} {1} with {2} wheels is now off, and it has a max speed of {3}", sColor, sModel, sWheels, sSpeed);
            }
            else{
                //System.Console.WriteLine("The {0} {1} with {2} wheels is now on, and it has a max speed of {3}", sColor, sModel, sWheels, sSpeed);
                System.Console.WriteLine("Turning engine on...");
                Thread.Sleep(1000);
                System.Console.WriteLine(".................");
                Thread.Sleep(1000);
                System.Console.WriteLine("..............");
                Thread.Sleep(1000);
                System.Console.WriteLine("...........");
                Thread.Sleep(1000);
                System.Console.WriteLine("It's cold");
                Thread.Sleep(1000);
                System.Console.WriteLine(".......");
                Thread.Sleep(1000);
                System.Console.WriteLine("Let's roll!");
            }
            
        
        }
        public void Characteristics(out string vModel, out string vColor, out int vWheels){
            Console.Clear();
            
            

            System.Console.WriteLine("Type the model of the vehicle: ");
            vModel = Console.ReadLine();
            System.Console.WriteLine("Type the color of the {0}: ", vModel);
            vColor = Console.ReadLine();
            System.Console.WriteLine("Type the numbers of wheels on the {0} {1}: ", vModel, vColor);

            vWheels = System.Convert.ToInt32(Console.ReadLine());
            

            
        }
    }
}