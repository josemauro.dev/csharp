﻿using System;
using System.Threading;

    namespace Stopwatch{
        class Program{
            static void Main(){
                Menu();
                
            }

            static void Menu(){
                int time;
                Console.Clear();
                Console.WriteLine("S = Second | M = Minute | 0 = Exit");
                Console.WriteLine("10s = 10 seconds | 10m = 10 minutes");
                Console.WriteLine("How long do you want to count?\n");
                
                string data = Console.ReadLine().ToLower();
                char type = char.Parse(data.Substring(data.Length-1,1));
                if(data.Length == 1){
                      time = int.Parse(data.Substring(0,1)) ;
                }
                else{
                     time = int.Parse(data.Substring(0,data.Length-1)) ;
                }
                 

                int multiplier = 1;
                if(type == 'm'){
                    multiplier = 60;
                }

                if(time == 0){
                    System.Environment.Exit(0);
                }


                Start(time*multiplier);
            }

            static void Start(int time){
                int currentTime=0;

                while(currentTime!=time){
                    Console.Clear();
                    currentTime ++;
                    Console.WriteLine(""+currentTime);
                    Thread.Sleep(1000);
                    
                }
                Console.Clear();
                Console.WriteLine("Stopwatch ended\n");
                Console.WriteLine("Returning to menu\n");
                Thread.Sleep(2500);
                Menu();
            }
        }
    }