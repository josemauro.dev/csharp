using System;

namespace MeuApp{
    class Program{
        
        static void Main(String[] args){
            Produto televisao = new Produto();
            string precoEmDolar="";
            televisao.ID=1;
            televisao.Nome="Samsung";
            televisao.Price=999.99;

            Console.WriteLine(televisao.ID);
            Console.WriteLine(televisao.Nome);
            Console.WriteLine(televisao.Price);

            precoEmDolar = televisao.PrecoDolar(televisao.Price);
            Console.WriteLine(precoEmDolar);
        }
    }

    struct Produto{
        public Produto(int identificador, string nomeProd, double precoProd){
            ID = identificador;
            Nome = nomeProd;
            Price = precoProd;

        }
    public int ID;
    public string Nome;
    public double Price;

        public string PrecoDolar(double price){
            double precoDolar = price*5.50;
            precoDolar = Math.Round(precoDolar,2);
            return "O preço em Dólar é: /n"+precoDolar;
        }
    
    }
}
